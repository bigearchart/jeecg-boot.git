
#### 介绍
基于jeecg开发的小程序商城项目，代码规范遵循阿里规范，只需要替换相关参数即可使用或二开，无任何隐藏代码

#### 软件架构
软件架构说明

语言：Java 8

IDE(JAVA)： IDEA / Eclipse安装lombok插件


依赖管理：Maven

数据库：MySQL5.7+ & Oracle 11g & Sqlserver2017

数据库版本管理：Flyway

缓存：Redis

基础框架：Spring Boot 2.3.5.RELEASE

微服务框架： Spring Cloud Alibaba 2.2.3.RELEASE

持久层框架：Mybatis-plus 3.4.1

安全框架：Apache Shiro 1.7.0，Jwt 3.11.0

微服务技术栈：Spring Cloud Alibaba、Nacos、Gateway、Sentinel、Skywarking

数据库连接池：阿里巴巴Druid 1.1.22

缓存框架：redis

日志打印：logback

其他：fastjson，poi，Swagger-ui，quartz, lombok（简化代码）等。


后台开发环境依赖

maven

jdk8

mysql

redis

数据库脚本：/db/jeecgboot-mysql-5.7.sql

默认登录账号： admin/123456


#### 使用说明

拉取项目代码
git clone https://gitee.com/bigearchart/jeecg-boot.git

maven的settings.xml文件使用jeecg私库


IDEA引入对应的maven配置，下载对应依赖，启动项目

具体操作可查看官方文档
http://doc.jeecg.com/2043873


#### 参与贡献
如果想一起加入我们，请发送邮件或者微信联系
邮箱:bigearchart@163.com
微信:bigearchart

