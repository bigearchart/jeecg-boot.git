/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80023
 Source Host           : localhost:3306
 Source Schema         : jeecg-boot

 Target Server Type    : MySQL
 Target Server Version : 80023
 File Encoding         : 65001

 Date: 12/04/2021 14:09:04
*/

SET NAMES utf8mb4;
SET
FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for shop_info
-- ----------------------------
DROP TABLE IF EXISTS `shop_info`;
CREATE TABLE `shop_info`
(
    `id`          varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL,
    `create_by`   varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
    `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
    `update_by`   varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
    `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日期',
    `shop_name`   varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商铺名称',
    `shop_type`   varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '商铺类型(0-线上，1-线下)',
    `link_man`    varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '联系人',
    `link_phone`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL COMMENT '联系电话',
    `link_email`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系邮箱',
    `id_card`     varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系人身份证号',
    `real_name`   varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '真实姓名',
    `sort`        int(0) NULL DEFAULT 0 COMMENT '排序字段(大-小)',
    `del_flag`   int(0) NULL DEFAULT 0 COMMENT '删除状态(0-正常,1-已删除)',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET
FOREIGN_KEY_CHECKS = 1;

/*添加商铺管理菜单以及操作按钮权限*/
INSERT INTO `sys_permission` VALUES ('1381514874907959298', '', '商铺管理', '/shop', 'layouts/RouteView', NULL, NULL, 0, NULL, '1', 4.00, 0, 'shop', 1, 0, 0, 0, NULL, 'admin', '2021-04-12 15:49:48', 'admin', '2021-04-12 15:50:08', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1381516059924668417', '1381514874907959298', '商铺列表', '/shop/ShopInfoList', 'shop/ShopInfoList', NULL, NULL, 1, NULL, '1', 1.00, 0, NULL, 1, 0, 0, 0, NULL, 'admin', '2021-04-12 15:54:31', NULL, NULL, 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1381516259959414785', '1381516059924668417', '添加权限', NULL, NULL, NULL, NULL, 2, 'shopInfo:add', '1', 1.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2021-04-12 15:55:18', NULL, NULL, 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1381516364728934402', '1381516059924668417', '修改权限', NULL, NULL, NULL, NULL, 2, 'shopInfo:edit', '1', 1.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2021-04-12 15:55:43', NULL, NULL, 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1381516568953790466', '1381516059924668417', '导入权限', NULL, NULL, NULL, NULL, 2, 'shopInfo:import', '1', 1.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2021-04-12 15:56:32', NULL, NULL, 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1381516689649082369', '1381516059924668417', '导出权限', NULL, NULL, NULL, NULL, 2, 'shopInfo:export', '1', 1.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2021-04-12 15:57:01', NULL, NULL, 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1381516689649082370', '1381516059924668417', '删除权限', NULL, NULL, NULL, NULL, 2, 'shopInfo:delete', '1', 1.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2021-04-12 15:57:01', NULL, NULL, 0, 0, '1', 0);

/*数据字典新增*/
INSERT INTO `sys_dict` VALUES ('1381525795046879233', '商铺类型', 'shop_type', '商铺类型', 0, 'admin', '2021-04-12 16:33:12', NULL, NULL, 0);
INSERT INTO `sys_dict_item` VALUES ('1381525868447199234', '1381525795046879233', '线上', '0', '商铺类型-线上', 1, 1, 'admin', '2021-04-12 16:33:29', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('1381525918279725058', '1381525795046879233', '线下', '1', '商铺类型-线下', 2, 1, 'admin', '2021-04-12 16:33:41', NULL, NULL);