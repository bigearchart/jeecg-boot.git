-- ----------------------------
-- Table structure for trade_area
-- ----------------------------
DROP TABLE IF EXISTS `trade_area`;
CREATE TABLE `trade_area`
(
    `id`                     varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
    `create_by`              varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
    `create_time`            datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
    `update_by`              varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
    `update_time`            datetime(0) NULL DEFAULT NULL COMMENT '更新日期',
    `name`                   varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商圈名称',
    `longitude_and_latitude` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '经纬度',
    `sort`                   int(0) NULL DEFAULT 0 COMMENT '排序',
    `del_flag`               varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '是否删除(0-否 1-是)',
    `test_flyway_auto`       varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '测试flyway自动维护字段差异',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX                    `name`(`name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of trade_area
-- ----------------------------
INSERT INTO `trade_area`
VALUES ('1371287345785954305', 'admin', '2021-03-15 10:29:15', 'admin', '2021-03-15 13:52:27', '万达广场', '1982.566.2885',
        0, '1', NULL);
INSERT INTO `trade_area`
VALUES ('1371338524993773569', 'admin', '2021-03-15 13:52:37', 'admin', '2021-03-15 13:54:33', '万达广场', NULL, 0, '0',
        NULL);
INSERT INTO `trade_area`
VALUES ('1371339602040598529', 'admin', '2021-03-15 13:56:54', 'admin', '2021-03-15 13:57:02', '万达广场2', NULL, 0, '0',
        NULL);

SET
FOREIGN_KEY_CHECKS = 1;