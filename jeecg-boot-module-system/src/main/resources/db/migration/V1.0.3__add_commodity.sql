/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80023
 Source Host           : localhost:3306
 Source Schema         : jeecg-boot

 Target Server Type    : MySQL
 Target Server Version : 80023
 File Encoding         : 65001

 Date: 12/04/2021 13:37:18
*/

SET NAMES utf8mb4;
SET
FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for commodity_info
-- ----------------------------
DROP TABLE IF EXISTS `commodity_info`;
CREATE TABLE `commodity_info`
(
    `id`            varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL,
    `create_by`     varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
    `create_time`   datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
    `update_by`     varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
    `update_time`   datetime(0) NULL DEFAULT NULL COMMENT '更新日期',
    `name`          varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品名称',
    `type_id`       varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL COMMENT '商品类型',
    `code`          varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品编码',
    `give_integral` int(0) NULL DEFAULT 0 COMMENT '赠送积分',
    `preferential`  varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '是否优惠(0-否，1-是)',
    `cost_price`    decimal(20, 2) NULL DEFAULT 0.00 COMMENT '商品原价',
    `ruling_price`  decimal(10, 2)                                          NOT NULL DEFAULT 0.00 COMMENT '商品现价',
    `surplus_stock` int(0) NOT NULL DEFAULT 0 COMMENT '剩余库存',
    `freeze_stock`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '冻结库存',
    `logo_url`      varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品logo',
    `synopsis`      longblob NULL COMMENT '商品介绍',
    `status`        varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '商品状态',
    `sort`          int(0) NULL DEFAULT 0 COMMENT '排序字段(大-小)',
    `del_flag`      int(0) NULL DEFAULT 0 COMMENT '删除状态(0-正常,1-已删除)',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for commodity_type
-- ----------------------------
DROP TABLE IF EXISTS `commodity_type`;
CREATE TABLE `commodity_type`
(
    `id`          varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL,
    `create_by`   varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
    `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
    `update_by`   varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
    `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日期',
    `name`        varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '类型名称',
    `code`        varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL COMMENT '类型编码',
    `del_flag`    int(0) NULL DEFAULT 0 COMMENT '删除状态(0-正常,1-已删除)',
    `sort`        int(0) NULL DEFAULT NULL COMMENT '排序(大到小)',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX         `name`(`name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

ALTER TABLE `commodity_info`
    ADD COLUMN `shop_id` varchar (50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属商铺' AFTER `del_flag`;

/*商圈菜单*/
INSERT INTO `sys_permission` VALUES ('1370299228283158530', '', '商圈管理', '/tradearea', 'layouts/RouteView', NULL, NULL, 0, NULL, '1', 3.00, 0, 'home', 1, 0, 0, 0, NULL, 'admin', '2021-03-12 17:02:50', 'admin', '2021-04-12 16:20:41', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1370299606831677441', '1370299228283158530', '商圈列表', '/tradeArea/TradeAreaList', 'tradeArea/TradeAreaList', NULL, NULL, 1, NULL, '1', 1.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2021-03-12 17:04:20', NULL, NULL, 0, 0, '1', 0);
