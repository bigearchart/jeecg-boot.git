package org.jeecg.modules.tradearea.controller.emp;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.tradearea.service.ITradeAreaService;
import org.jeecg.modules.tradearea.entity.TradeArea;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

/**
 * @Description: 商圈信息-控制层
 * @Author: bigearchart
 * @Date: 2021/3/15
 * @return
 */
@Api(tags="商圈信息")
@RestController
@RequestMapping("/emp/tradeArea")
public class EmpTradeAreaController extends JeecgController<TradeArea, ITradeAreaService> {
	@Resource
	private ITradeAreaService tradeAreaService;

	 /**
	  * 商圈信息-列表查询
	  * @param tradeArea --- 查询条件
	  * @param pageNo --- 当前页
	  * @param pageSize --- 最大条数
	  * @param req --- 返回结果
	  * @return
	  */
	@AutoLog(value = "商圈信息-列表查询")
	@ApiOperation(value="商圈信息-列表查询", notes="商圈信息-列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(TradeArea tradeArea,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		return tradeAreaService.queryPageList(tradeArea, pageNo, pageSize, req);
	}

	/**
	 * 商圈信息-新增商圈
	 * @param tradeArea --- 商圈参数
	 * @return
	 */
	@AutoLog(value = "商圈信息-新增商圈")
	@ApiOperation(value="商圈信息-新增商圈", notes="商圈信息-新增商圈")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody TradeArea tradeArea) {
		return tradeAreaService.add(tradeArea);
	}

	/**
	 * 商圈信息-修改商圈
	 * @param tradeArea --- 商圈参数
	 * @return
	 */
	@AutoLog(value = "商圈信息-修改商圈")
	@ApiOperation(value="商圈信息-修改商圈", notes="商圈信息-修改商圈")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody TradeArea tradeArea) {
		return tradeAreaService.edit(tradeArea);
	}

	/**
	 * 商圈信息-根据id删除商圈
	 * @param id --- 商圈id
	 * @return
	 */
	@AutoLog(value = "商圈信息-根据id删除商圈")
	@ApiOperation(value="商圈信息-根据id删除商圈", notes="商圈信息-根据id删除商圈")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		return tradeAreaService.delete(id);
	}

	/**
	 * 商圈信息-批量删除商圈
	 * @param ids --- 商圈id数组
	 * @return
	 */
	@AutoLog(value = "商圈信息-批量删除商圈")
	@ApiOperation(value="商圈信息-批量删除商圈", notes="商圈信息-批量删除商圈")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		return tradeAreaService.deleteBatch(ids);
	}

	/**
	 * 商圈信息-根据id查询商圈信息
	 * @param id --- 商圈id
	 * @return
	 */
	@AutoLog(value = "商圈信息-通过id查询")
	@ApiOperation(value="商圈信息-通过id查询", notes="商圈信息-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		return tradeAreaService.queryById(id);
	}

	/**
	 * 商圈信息-导出excel
	 * @param request
	 * @param tradeArea
	 * @return
	 */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, TradeArea tradeArea) {
        return super.exportXls(request, tradeArea, TradeArea.class, "商圈信息");
    }

	/**
	 * 商圈信息-导入excel
	 * @param request
	 * @param response
	 * @return
	 */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, TradeArea.class);
    }

}
