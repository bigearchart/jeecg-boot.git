package org.jeecg.modules.tradearea.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.tradearea.entity.TradeArea;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 商圈信息-数据接口定义
 * @Author: bigearchart
 * @Date: 2021/3/15
 * @return
 */
@Mapper
public interface TradeAreaMapper extends BaseMapper<TradeArea> {

    /**
     * 商圈信息-根据商圈名称验证是否存在
     * @param name --- 商圈名称
     * @return
     */
    TradeArea selectByName(@Param(value = "name") String name);
}
