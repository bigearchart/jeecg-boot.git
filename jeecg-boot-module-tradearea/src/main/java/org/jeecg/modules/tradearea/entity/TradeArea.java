package org.jeecg.modules.tradearea.entity;

import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 商圈信息-实体类
 * @Author: bigearchart
 * @Date: 2021/4/12
 * @return
 */
@Data
@TableName("trade_area")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="trade_area对象", description="商圈信息")
public class TradeArea implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;

	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;

	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;

	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;

	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;

	/**商圈名称*/
	@Excel(name = "商圈名称", width = 15)
    @ApiModelProperty(value = "商圈名称")
    private java.lang.String name;

	/**经纬度*/
	@Excel(name = "经纬度", width = 15)
    @ApiModelProperty(value = "经纬度")
    private java.lang.String longitudeAndLatitude;

	/**排序*/
	@Excel(name = "排序", width = 15)
    @ApiModelProperty(value = "排序")
    private java.lang.Integer sort;

	/**是否删除(0-否 1-是)*/
	@Excel(name = "是否删除(0-否 1-是)", width = 15)
    @ApiModelProperty(value = "是否删除(0-否 1-是)")
    private java.lang.String delFlag;
}
