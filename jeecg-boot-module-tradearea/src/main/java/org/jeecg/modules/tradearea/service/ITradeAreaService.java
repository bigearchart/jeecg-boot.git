package org.jeecg.modules.tradearea.service;

import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.tradearea.entity.TradeArea;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.stereotype.Service;
import javax.servlet.http.HttpServletRequest;

/**
 * @Description: 商圈信息-业务接口定义
 * @Author: bigearchart
 * @Date: 2021/3/15
 * @return
 */
@Service
public interface ITradeAreaService extends IService<TradeArea> {


    /**
     * 商圈信息-列表查询
     * @param tradeArea --- 查询条件
     * @param pageNo --- 当前页
     * @param pageSize --- 最大条数
     * @param req --- 返回结果
     * @return
     */
    public Result<?> queryPageList(TradeArea tradeArea,Integer pageNo,Integer pageSize,
                                   HttpServletRequest req);

    /**
     * 商圈信息-根据id查询商圈信息
     * @param id --- 商圈id
     * @return
     */
    public Result<?> queryById(String id);

    /**
     * 商圈信息-新增商圈
     * @param tradeArea --- 商圈参数
     * @return
     */
    public Result<?> add(TradeArea tradeArea);

    /**
     * 商圈信息-修改商圈
     * @param tradeArea --- 商圈参数
     * @return
     */
    public Result<?> edit(TradeArea tradeArea);

    /**
     * 商圈信息-根据id删除商圈
     * @param id --- 商圈id
     * @return
     */
    public Result<?> delete(String id);

    /**
     * 商圈信息-批量删除商圈
     * @param ids --- 商圈id数组
     * @return
     */
    public Result<?> deleteBatch(String ids);
}
