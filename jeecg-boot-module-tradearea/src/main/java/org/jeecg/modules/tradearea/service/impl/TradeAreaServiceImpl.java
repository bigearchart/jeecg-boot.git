package org.jeecg.modules.tradearea.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.tradearea.service.ITradeAreaService;
import org.jeecg.modules.tradearea.entity.TradeArea;
import org.jeecg.modules.tradearea.mapper.TradeAreaMapper;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;

/**
 * @Description: 商圈信息-业务接口实现类
 * @Author: bigearchart
 * @Date: 2021/3/15
 * @return
 */
@Service
@Slf4j
public class TradeAreaServiceImpl extends ServiceImpl<TradeAreaMapper, TradeArea> implements ITradeAreaService {

    @Resource
    private TradeAreaMapper tradeAreaMapper;

    /**
     * 商圈信息-列表查询
     * @param tradeArea --- 查询条件
     * @param pageNo --- 当前页
     * @param pageSize --- 最大条数
     * @param req --- 返回结果
     * @return
     */
    @Override
    public Result<?> queryPageList(TradeArea tradeArea, Integer pageNo, Integer pageSize, HttpServletRequest req) {
        //设置查询未删除状态
        tradeArea.setDelFlag(CommonConstant.DEL_FLAG_0.toString());
        //创建查询过滤器
        QueryWrapper<TradeArea> queryWrapper = QueryGenerator.initQueryWrapper(tradeArea, req.getParameterMap());
        //封装分页参数
        Page<TradeArea> page = new Page<TradeArea>(pageNo, pageSize);
        //根据过滤器分页查询
        IPage<TradeArea> pageList = page(page, queryWrapper);
        return Result.OK(pageList);
    }

    /**
     * 商圈信息-根据id查询商圈信息
     * @param id --- 商圈id
     * @return
     */
    @Override
    public Result<?> queryById(String id) {
        TradeArea tradeArea = getById(id);
        if(tradeArea==null) {
            return Result.error("未找到对应数据");
        }
        return Result.OK(tradeArea);
    }
    /**
     * 商圈信息-新增商圈
     * @param tradeArea --- 商圈参数
     * @return
     */
    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public Result<?> add(TradeArea tradeArea) {
        try {
            //验证商圈名称是否非空
            if(StrUtil.isEmpty(tradeArea.getName())){
                throw new RuntimeException("商圈名称不可为空");
            }
            if(tradeAreaMapper.selectByName(tradeArea.getName()) != null){
                throw new RuntimeException("商圈名称已存在");
            }
            tradeAreaMapper.insert(tradeArea);
            return Result.OK("添加成功!");
        }catch (Exception e){
            log.error(e.getMessage(), e);
            //手动回滚事务
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();

            return  Result.error(e.getMessage());
        }
    }
    /**
     * 商圈信息-修改商圈
     * @param tradeArea --- 商圈参数
     * @return
     */
    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public Result<?> edit(TradeArea tradeArea) {
        try {
            //验证id是否为空
            if(StrUtil.isEmpty(tradeArea.getId())){
                throw new RuntimeException("商圈id不可为空");
            }
            //验证商圈名称是否非空
            if(StrUtil.isEmpty(tradeArea.getName())){
                throw new RuntimeException("商圈名称不可为空");
            }
            TradeArea area = getById(tradeArea.getId());
            if(area == null){
                throw new RuntimeException("商圈不存在");
            }
            //验证商圈名称是否修改名称
            if(!area.getName().equals(tradeArea.getName())){
                //如果修改名称则验证商圈名称是否重复
                if(tradeAreaMapper.selectByName(tradeArea.getName()) != null){
                    throw new RuntimeException("商圈名称已存在");
                }
            }
            tradeAreaMapper.updateById(tradeArea);
            return Result.OK("修改成功!");
        }catch (Exception e){
            log.error(e.getMessage(), e);
            //手动回滚事务
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();

            return  Result.error(e.getMessage());
        }
    }

    /**
     * 商圈信息-根据id删除商圈
     * @param id --- 商圈id
     * @return
     */
    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public Result<?> delete(String id) {
        try {
            TradeArea area = getById(id);
            if(area == null){
                throw new RuntimeException("商圈不存在");
            }
            //设置删除状态为已删除
            area.setDelFlag(CommonConstant.DEL_FLAG_1.toString());
            tradeAreaMapper.updateById(area);

            return Result.OK("删除成功!");
        }catch (Exception e){
            log.error(e.getMessage(), e);
            //手动回滚事务
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();

            return  Result.error(e.getMessage());
        }
    }

    /**
     * 商圈信息-批量删除商圈
     * @param ids --- 商圈id数组
     * @return
     */
    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public Result<?> deleteBatch(String ids) {
        try {
            //将数组序列号为单个id集合
            Collection<? extends Serializable> idList = Arrays.asList(ids.split(","));
            //循环id集合
            for (Serializable id: idList) {
                TradeArea area = getById(id);
                if(area == null){
                    throw new RuntimeException("商圈不存在");
                }
                //设置删除状态为已删除
                area.setDelFlag(CommonConstant.DEL_FLAG_1.toString());
                tradeAreaMapper.updateById(area);
            }
            return Result.OK("删除成功!");
        }catch (Exception e){
            log.error(e.getMessage(), e);
            //手动回滚事务
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();

            return  Result.error(e.getMessage());
        }
    }
}
