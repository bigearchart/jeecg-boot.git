package org.jeecg.modules.shop.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.shop.entity.ShopInfo;

/**
 * @Description: 商铺信息-数据接口定义
 * @Author: bigearchart
 * @Date: 2021/4/12
 * @return
 */
public interface ShopInfoMapper extends BaseMapper<ShopInfo> {

    /**
     * 商铺信息-根据商铺名称查询
     * @param name
     * @return
     */
    public ShopInfo selectByName(@Param(value = "name") String name);

}
