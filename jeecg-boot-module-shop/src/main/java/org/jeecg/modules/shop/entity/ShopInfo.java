package org.jeecg.modules.shop.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 商铺信息实体类
 * @Author: bigearchart
 * @Date: 2021/4/12
 * @return
 */
@Data
@TableName("shop_info")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="shop_info对象", description="商铺信息")
public class ShopInfo implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private String id;

	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private String createBy;

	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private Date createTime;

	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private String updateBy;

	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private Date updateTime;

	/**商铺名称*/
	@Excel(name = "商铺名称", width = 15)
    @ApiModelProperty(value = "商铺名称")
    private String shopName;

	/**商铺类型(0-线上，1-线下)*/
	@Excel(name = "商铺类型", width = 15,dicCode = "shop_type")
    @ApiModelProperty(value = "商铺类型")
    @Dict(dicCode = "shop_type")
    private String shopType;

	/**联系人*/
	@Excel(name = "联系人", width = 15)
    @ApiModelProperty(value = "联系人")
    private String linkMan;

	/**联系电话*/
	@Excel(name = "联系电话", width = 15)
    @ApiModelProperty(value = "联系电话")
    private String linkPhone;

	/**联系邮箱*/
	@Excel(name = "联系邮箱", width = 15)
    @ApiModelProperty(value = "联系邮箱")
    private String linkEmail;

	/**联系人身份证号*/
	@Excel(name = "联系人身份证号", width = 15)
    @ApiModelProperty(value = "联系人身份证号")
    private String idCard;

	/**真实姓名*/
	@Excel(name = "真实姓名", width = 15)
    @ApiModelProperty(value = "真实姓名")
    private String realName;

	/**排序字段(大-小)*/
	@Excel(name = "排序字段", width = 15)
    @ApiModelProperty(value = "排序字段")
    private Integer sort;

	/**删除状态(0-正常,1-已删除)*/
    @ApiModelProperty(value = "删除状态")
    private Integer delFlag;
}
