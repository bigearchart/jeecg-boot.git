package org.jeecg.modules.shop.controller.emp;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.modules.shop.entity.ShopInfo;
import org.jeecg.modules.shop.service.IShopInfoService;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

/**
 * @Description: 商铺信息-控制层
 * @Author: bigearchart
 * @Date: 2021/4/12
 * @return
 */
@Api(tags="商铺信息")
@RestController
@RequestMapping("/emp/shopInfo")
@Slf4j
public class EmpShopInfoController extends JeecgController<ShopInfo, IShopInfoService> {
	@Autowired
	private IShopInfoService shopInfoService;

	/**
	 * 商铺信息-列表查询
	 * @param shopInfo --- 查询条件
	 * @param pageNo --- 当前页
	 * @param pageSize --- 最大条数
	 * @param req --- 返回结果
	 * @return
	 */
	@AutoLog(value = "商铺信息-列表查询")
	@ApiOperation(value="商铺信息-列表查询", notes="商铺信息-列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(ShopInfo shopInfo,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		return shopInfoService.queryPageList(shopInfo, pageNo, pageSize, req);
	}

	/**
	 * 商铺信息-根据id查询商铺信息
	 * @param id --- 商铺id
	 * @return
	 */
	@AutoLog(value = "商铺信息-根据id查询商铺信息")
	@ApiOperation(value="商铺信息-根据id查询商铺信息", notes="商铺信息-根据id查询商铺信息")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		return shopInfoService.queryById(id);
	}

	/**
	 * 商铺信息-新增商铺
	 * @param shopInfo --- 商铺参数
	 * @return
	 */
	@AutoLog(value = "商铺信息-新增商铺")
	@ApiOperation(value="商铺信息-新增商铺", notes="商铺信息-新增商铺")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody ShopInfo shopInfo) {
		return shopInfoService.add(shopInfo);
	}

	/**
	 * 商铺信息-修改商铺
	 * @param shopInfo --- 商铺参数
	 * @return
	 */
	@AutoLog(value = "商铺信息-修改商铺")
	@ApiOperation(value="商铺信息-修改商铺", notes="商铺信息-修改商铺")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody ShopInfo shopInfo) {
		return shopInfoService.edit(shopInfo);
	}

	/**
	 * 商铺信息-根据id删除商铺
	 * @param id --- 商铺id
	 * @return
	 */
	@AutoLog(value = "商铺信息-根据id删除商铺")
	@ApiOperation(value="商铺信息-根据id删除商铺", notes="商铺信息-根据id删除商铺")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		return shopInfoService.delete(id);
	}

	/**
	 * 商铺信息-批量删除商铺
	 * @param ids --- 商铺id集合
	 * @return
	 */
	@AutoLog(value = "商铺信息-批量删除商铺")
	@ApiOperation(value="商铺信息-批量删除商铺", notes="商铺信息-批量删除商铺")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		return shopInfoService.deleteBatch(ids);
	}

	/**
	 * 商铺信息-导出excel
	 * @param request
	 * @param shopInfo
	 * @return
	 */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ShopInfo shopInfo) {
        return super.exportXls(request, shopInfo, ShopInfo.class, "商铺信息");
    }

	/**
	 * 商铺信息-导入excel
	 * @param request
	 * @param response
	 * @return
	 */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ShopInfo.class);
    }

}
