package org.jeecg.modules.shop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.shop.entity.ShopInfo;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

/**
 * @Description: 商铺信息-业务接口定义
 * @Author: bigearchart
 * @Date: 2021/4/12
 * @return
 */
public interface IShopInfoService extends IService<ShopInfo> {

    /**
     * 商铺信息-列表查询
     * @param shopInfo --- 查询条件
     * @param pageNo --- 当前页
     * @param pageSize --- 最大条数
     * @param req --- 返回结果
     * @return
     */
    public Result<?> queryPageList(ShopInfo shopInfo,Integer pageNo,Integer pageSize,HttpServletRequest req);

    /**
     * 商铺信息-根据id查询商铺信息
     * @param id --- 商铺id
     * @return
     */
    public Result<?> queryById(String id);

    /**
     * 商铺信息-新增商铺
     * @param shopInfo --- 商铺参数
     * @return
     */
    public Result<?> add(ShopInfo shopInfo);


    /**
     * 商铺信息-修改商铺
     * @param shopInfo --- 商铺参数
     * @return
     */
    public Result<?> edit(@RequestBody ShopInfo shopInfo);

    /**
     * 商铺信息-根据id删除商铺
     * @param id --- 商铺id
     * @return
     */
    public Result<?> delete(String id);

    /**
     * 商铺信息-批量删除商铺
     * @param ids --- 商铺id集合
     * @return
     */
    public Result<?> deleteBatch(String ids);
}
