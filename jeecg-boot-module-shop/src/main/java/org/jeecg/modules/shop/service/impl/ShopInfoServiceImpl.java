package org.jeecg.modules.shop.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.shop.entity.ShopInfo;
import org.jeecg.modules.shop.mapper.ShopInfoMapper;
import org.jeecg.modules.shop.service.IShopInfoService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;

/**
 * @Description: 商铺信息-业务接口实现类
 * @Author: bigearchart
 * @Date: 2021/4/12
 * @return
 */
@Service
public class ShopInfoServiceImpl extends ServiceImpl<ShopInfoMapper, ShopInfo> implements IShopInfoService {

    @Resource
    private ShopInfoMapper shopInfoMapper;

    /**
     * 商铺信息-列表查询
     * @param shopInfo --- 查询条件
     * @param pageNo --- 当前页
     * @param pageSize --- 最大条数
     * @param req --- 返回结果
     * @return
     */
    @Override
    public Result<?> queryPageList(ShopInfo shopInfo, Integer pageNo, Integer pageSize, HttpServletRequest req) {
        //设置查询未删除状态
        shopInfo.setDelFlag(CommonConstant.DEL_FLAG_0);
        //创建查询过滤器
        QueryWrapper<ShopInfo> queryWrapper = QueryGenerator.initQueryWrapper(shopInfo, req.getParameterMap());
        //封装分页参数
        Page<ShopInfo> page = new Page<ShopInfo>(pageNo, pageSize);
        //根据过滤器分页查询
        IPage<ShopInfo> pageList = page(page, queryWrapper);
        return Result.OK(pageList);
    }

    /**
     * 商铺信息-根据id查询商铺信息
     * @param id --- 商铺id
     * @return
     */
    @Override
    public Result<?> queryById(String id) {
        ShopInfo shopInfo = getById(id);
        if(shopInfo==null) {
            return Result.error("未找到对应数据");
        }
        return Result.OK(shopInfo);
    }

    /**
     * 商铺信息-新增商铺
     * @param shopInfo --- 商铺参数
     * @return
     */
    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public Result<?> add(ShopInfo shopInfo) {
        try {
            //验证商铺名称是否非空
            if(StrUtil.isEmpty(shopInfo.getShopName())){
                throw new RuntimeException("商铺名称不可为空");
            }
            //验证商铺名称是否存在
            if(shopInfoMapper.selectByName(shopInfo.getShopName()) != null){
                throw new RuntimeException("商铺名称已存在");
            }
            shopInfoMapper.insert(shopInfo);
            return Result.OK("添加成功！");
        }catch (Exception e){
            log.error(e.getMessage(), e);
            //手动回滚事务
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();

            return  Result.error(e.getMessage());
        }
    }

    /**
     * 商铺信息-修改商铺
     * @param shopInfo --- 商铺参数
     * @return
     */
    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public Result<?> edit(ShopInfo shopInfo) {
        try {
            //验证商铺名称是否非空
            if(StrUtil.isEmpty(shopInfo.getShopName())){
                throw new RuntimeException("商铺名称不可为空");
            }
            //验证商铺是否存在
            ShopInfo info = shopInfoMapper.selectById(shopInfo.getId());
            if(info == null){
                throw new RuntimeException("商铺不存在");
            }
            //验证商铺是否修改名称
            if(!info.getShopName().equals(shopInfo.getShopName())){
                //如果修改名称则验证商铺名称是否存在
                if(shopInfoMapper.selectByName(shopInfo.getShopName()) != null){
                    throw new RuntimeException("商铺名称已存在");
                }
            }
            //保存商铺信息
            shopInfoMapper.updateById(shopInfo);
            return Result.OK("修改成功!");

        }catch (Exception e){
            log.error(e.getMessage(), e);
            //手动回滚事务
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();

            return  Result.error(e.getMessage());
        }
    }

    /**
     * 商铺信息-根据id删除商铺
     * @param id --- 商铺id
     * @return
     */
    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public Result<?> delete(String id) {
        try {
            //验证商铺是否存在
            ShopInfo info = shopInfoMapper.selectById(id);
            if(info == null){
                throw new RuntimeException("商铺不存在");
            }
            info.setDelFlag(CommonConstant.DEL_FLAG_1);
            //保存商铺信息
            shopInfoMapper.updateById(info);
            return Result.OK("删除成功!");

        }catch (Exception e){
            log.error(e.getMessage(), e);
            //手动回滚事务
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();

            return  Result.error(e.getMessage());
        }
    }

    /**
     * 商铺信息-批量删除商铺
     * @param ids --- 商铺id集合
     * @return
     */
    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public Result<?> deleteBatch(String ids) {
        try {
            //将数组序列号为单个id集合
            Collection<? extends Serializable> idList = Arrays.asList(ids.split(","));
            //循环id集合
            for (Serializable id: idList) {
                //验证商铺是否存在
                ShopInfo info = shopInfoMapper.selectById(id);
                if(info == null){
                    throw new RuntimeException("商铺不存在");
                }
                info.setDelFlag(CommonConstant.DEL_FLAG_1);
                //保存商铺信息
                shopInfoMapper.updateById(info);
            }
            return Result.OK("删除成功!");
        }catch (Exception e){
            log.error(e.getMessage(), e);
            //手动回滚事务
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();

            return  Result.error(e.getMessage());
        }
    }
}
